#!/bin/bash

keys_folder="keys"
cloudinit_file="terraform/modules/custom_vm_provisioning/cloudinit.txt"

if [ ! -d "$keys_folder" ]; then
    echo "Keys folder '$keys_folder' not found."
    exit 1
fi

authorized_keys_content=""
for pubkey_file in "$keys_folder"/*.pub; do
    if [ -f "$pubkey_file" ]; then
        pubkey_content=$(cat "$pubkey_file")
        authorized_keys_content+="        - $pubkey_content\n"
    fi
done

sed -i '/ssh_authorized_keys:/a \'"$authorized_keys_content" "$cloudinit_file"
sed -i "s/name: example/name: ${CLOUD_USER}/" "${cloudinit_file}"
