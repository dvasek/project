#!/bin/bash

terraform_backend_placement="terraform/main.tf"

sed -i "s/bucket = \"\"/bucket = \"${S3_BUCKET}\"/" "${terraform_backend_placement}"
sed -i "s/region = \"\"/region = \"${S3_REGION}\"/" "${terraform_backend_placement}"
sed -i "s/access_key = \"\"/access_key = \"${S3_ACCESS}\"/" "${terraform_backend_placement}"
sed -i "s/secret_key = \"\"/secret_key = \"${S3_SECRET}\"/" "${terraform_backend_placement}"
sed -i "s/endpoint = \"\"/endpoint = \"${S3_ENDPOINT}\"/" "${terraform_backend_placement}"

sed -i "s/provider_network = \"\"/provider_network = \"${PROVIDER_NETWORK}\"/" "${terraform_backend_placement}"
