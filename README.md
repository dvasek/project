# Custom VM provisioning

This project aims to deploy a virtual instance on OpenStack with preinstalled docker running postgresql and prometheus node_exporter.

## Terraform

The terraform subfolder contains `main.tf`, where you can change the VM flavor settings. However a suitable flavor should exist.

The parameters are:

### VM definition
```
vm_name    = "custom-vm"
vm_cores   = 2                      # vcpus
vm_ram     = 4096                   # ram in MB
vm_disk    = 20                     # disk storage in GB
vm_address = "10.0.0.4"             # address from CIDR 10.0.0.0/24
vm_ssh_key = "../keys/id_rsa.pub"   # path to public key for bootstrap
```

### Provider network
If you want to connect to the instance from outside of the cloud environment, specify variable `${PROVIDER_NETWORK}`, which contains the name of a suitable provider network in the cloud environment.

### Custom user
The terraform deploys a custom user via cloudinit.ini with root priviledge. The name of the user is configured with `${CLOUD_USER}` variable. The user is injected with ssh keys stored in keys folder.

## Ansible
The Ansible part of this project deploys Docker, Postgresql container and Prometheus Node Exporter.

## Deployment
In order to deploy the infrastructure, use the `deploy_vm` stage in CI/CD. To destroy the infrastructure, use `destroy_vm` stage in CI/CD.
