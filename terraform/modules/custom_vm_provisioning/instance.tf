resource "openstack_compute_instance_v2" "custom_instance" {
  name            = "${var.vm_name}"
  image_id        = data.openstack_images_image_v2.ubuntu.id
  flavor_id       = data.openstack_compute_flavor_v2.suitable_flavor.id
  key_pair        = openstack_compute_keypair_v2.custom_keypair.name
  security_groups = ["${var.infra}-sg"]
  user_data       = "#cloud-config\nhostname: ${var.vm_name}.local\n${file("${path.module}/cloudinit.txt")}"

  network {
    uuid = openstack_networking_network_v2.custom_network.id
    port = openstack_networking_port_v2.custom_port.id
  }
}

resource "openstack_networking_floatingip_v2" "floating_ip" {
  pool  = "${var.provider_network}"
}

resource "openstack_compute_floatingip_associate_v2" "fip_associate" {
  floating_ip = openstack_networking_floatingip_v2.floating_ip.address
  instance_id = openstack_compute_instance_v2.custom_instance.id
}
