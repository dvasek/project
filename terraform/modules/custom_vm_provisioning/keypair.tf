resource "openstack_compute_keypair_v2" "custom_keypair" {
  name = "${var.infra}-custom-keypair"
  public_key = file("${var.vm_ssh_key}")
}
