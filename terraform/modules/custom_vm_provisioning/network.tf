resource "openstack_networking_network_v2" "custom_network" {
  name           = "{var.infra}-network"
  admin_state_up = "true"
}

resource "openstack_networking_subnet_v2" "custom_subnet" {
  name       = "${var.infra}-subnet"
  network_id = openstack_networking_network_v2.custom_network.id
  cidr       = "${var.infra_network_cidr}"
  ip_version = 4
}

resource "openstack_compute_secgroup_v2" "custom_secgroup" {
  name        = "${var.infra}-sg"
  description = "Custom security group rules"

  rule {
    from_port   = 22
    to_port     = 22
    ip_protocol = "tcp"
    cidr        = "0.0.0.0/0"
  }

  rule {
    from_port   = 9182
    to_port     = 9182
    ip_protocol = "tcp"
    cidr        = "0.0.0.0/0"
  }
}

resource "openstack_networking_port_v2" "custom_port" {
  name               = "${var.infra}-port"
  network_id         = openstack_networking_network_v2.custom_network.id
  admin_state_up     = "true"
  security_group_ids = [openstack_compute_secgroup_v2.custom_secgroup.id]

  fixed_ip {
    subnet_id  = openstack_networking_subnet_v2.custom_subnet.id
    ip_address = "${var.vm_address}"
  }
}

data "openstack_networking_network_v2" "provider_network" {
  name = "${var.provider_network}"
}

resource "openstack_networking_router_v2" "custom_router" {
  name                = "${var.infra}-router"
  admin_state_up      = "true"
  external_network_id = data.openstack_networking_network_v2.provider_network.id
}

resource "openstack_networking_router_interface_v2" "router_interface" {
  router_id = openstack_networking_router_v2.custom_router.id
  subnet_id = openstack_networking_subnet_v2.custom_subnet.id
}
