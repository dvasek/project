data "openstack_compute_flavor_v2" "suitable_flavor" {
  vcpus     = "${var.vm_cores}"
  ram       = "${var.vm_ram}"
  min_disk  = "${var.vm_disk}"
}
