# variables for project scope

variable "infra" {
  default = "test"
}

variable "infra_network_cidr" {
  default = "10.0.0.0/24"
}

variable "provider_network" {
  description = "Name or ID of the provider public network for FIP association"
  default = ""
}

# custom variables for VM definition

variable "vm_name" {
  default = "custom-vm"
}

variable "vm_cores" {
  default = 2
}

variable "vm_ram" {
  default = 8192
}

variable "vm_disk" {
  default = 20
}

variable "vm_address" {
  description = "IP address of the virtual machine"
  default = "10.0.0.4"
}

variable "vm_ssh_key" {
  description = "SSH key to be used for VM deployment, by default will generate a new key"
  default = ""
}
