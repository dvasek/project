data "openstack_images_image_v2" "ubuntu" {
  name        = "ubuntu-jammy-x86_64"
  most_recent = true
}
