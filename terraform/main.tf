terraform {
  required_providers {
    openstack = {
      source  = "terraform-provider-openstack/openstack"
      version = "~> 1.51.1"
    }
  }

  backend "s3" {
    # will be loaded from env variables
    bucket = ""
    key    = "terraform.tfstate"
    region = ""
    endpoint = ""
    access_key = ""
    secret_key = ""
    skip_credentials_validation = true
    skip_region_validation = true
    force_path_style = true
  }
}

provider "openstack" {
  # will be loaded from env variables
}

module "custom_vm_provisioning" {
  source = "./modules/custom_vm_provisioning"

  # will be loaded from env variables
  provider_network = ""

  # vm definition
  vm_name    = "custom-vm"
  vm_cores   = 2                      # vcpus
  vm_ram     = 4096                   # ram in MB
  vm_disk    = 20                     # disk storage in GB
  vm_address = "10.0.0.4"             # address from CIDR 10.0.0.0/24
  vm_ssh_key = "../keys/id_rsa.pub"    # path to public key
}
